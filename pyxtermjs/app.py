#!/usr/bin/env python3
import argparse
from flask import Flask, render_template
from flask_socketio import SocketIO
import pty
import os
import subprocess
import select
import termios
import struct
import fcntl
import shlex


__version__ = "0.4.0.1"

app = Flask(__name__, template_folder=".", static_folder=".", static_url_path="")
app.config["SECRET_KEY"] = "mysecretkey"
app.config["fd"] = None
app.config["proj_name"] = None
socketio = SocketIO(app)



# --------------------------------------------------------------
app.config["fd"] = {} # client_num:fd
app.config["childpid"] = {}

def get_fd(client_num):
    return app.config["fd"].get(client_num, "invalid")

def set_fd(client_num, fd):
    app.config["fd"][client_num] = fd

def get_childpid(client_num):
    return app.config["childpid"].get(client_num, "invalid")

def set_childpid(client_num, childpid):
    app.config["childpid"][client_num] = childpid
# --------------------------------------------------------------


def set_winsize(fd, row, col, xpix=0, ypix=0):
    winsize = struct.pack("HHHH", row, col, xpix, ypix)
    fcntl.ioctl(fd, termios.TIOCSWINSZ, winsize)


def read_and_forward_pty_output(client_num):
    max_read_bytes = 1024 * 20
    while True:
        socketio.sleep(0.01)

        timeout_sec = 0
        (data_ready, _, _) = select.select([get_fd(client_num)], [], [], timeout_sec)
        if data_ready:
            output = os.read(get_fd(client_num), max_read_bytes).decode()
            socketio.emit("pty-output", {"output": output, "client_num": client_num}, namespace="/pty")


@app.route("/")
def index():
    return render_template("index.html", proj_name=app.config["proj_name"])


@socketio.on("pty-input", namespace="/pty")
def pty_input(data):
    """write to the child pty. The pty sees this as if you are typing in a real
    terminal.
    """
    data_encoded = data["input"].encode()

    # disable Ctrl-C
    if data_encoded == b'\x03': return   # do nothing

    # print("writing to ptd: %s" % data["input"])
    client_num = data["client_num"]
    os.write(get_fd(client_num), data_encoded)


@socketio.on("pty-user", namespace="/pty")
def pty_user(data):
    client_num = data["client_num"]

    """new client connected"""
    if get_childpid(client_num) != "invalid":
        # already started child process, don't start another
        return


    # use back previous fd
    if get_fd(client_num) != "invalid":
        fd = get_fd(client_num)
        child_pid = get_childpid(client_num)
    else:
        # create child process attached to a pty we can read from and write to
        (child_pid, fd) = pty.fork()

    if child_pid == 0:
        # this is the child process fork.
        # anything printed here will show up in the pty, including the output
        # of this subprocess
        subprocess.run(app.config["cmd"])
    else:
        # this is the parent process fork.
        # store child fd and pid
        set_fd(client_num, fd)
        set_childpid(client_num, child_pid)

        #app.config["fd"] = fd
        #app.config["child_pid"] = child_pid
        set_winsize(fd, 50, 50)
        cmd = " ".join(shlex.quote(c) for c in app.config["cmd"])
        print("child pid is", child_pid)
        print(
            f"starting background task with command `{cmd}` to continously read "
            "and forward pty output to client"
        )
        socketio.start_background_task(read_and_forward_pty_output, client_num)


@socketio.on("resize", namespace="/pty")
def resize(data):
    if get_fd(data["client_num"]) != "invalid":
        set_winsize(get_fd(data["client_num"]), data["rows"], data["cols"])




@socketio.on("pty-disconnect", namespace="/pty")
def pty_disconnect(data):
    print("Disconnected: ", data["client_num"])


@socketio.on("disconnect", namespace="/pty")
def disconnect():
    print("Bye")


@socketio.on("connect", namespace="/pty")
def connect():

    return
    '''
    """new client connected"""
    if app.config["child_pid"]:
        # already started child process, don't start another
        return

    # create child process attached to a pty we can read from and write to
    (child_pid, fd) = pty.fork()
    if child_pid == 0:
        # this is the child process fork.
        # anything printed here will show up in the pty, including the output
        # of this subprocess
        subprocess.run(app.config["cmd"])
    else:
        # this is the parent process fork.
        # store child fd and pid

        app.config["fd"] = fd
        app.config["child_pid"] = child_pid
        set_winsize(fd, 50, 50)
        cmd = " ".join(shlex.quote(c) for c in app.config["cmd"])
        print("child pid is", child_pid)
        print(
            f"starting background task with command `{cmd}` to continously read "
            "and forward pty output to client"
        )
        #socketio.start_background_task(target=read_and_forward_pty_output)
        print("task started")
    '''

def main():
    parser = argparse.ArgumentParser(
        description=(
            "A fully functional terminal in your browser. "
            "https://github.com/cs01/pyxterm.js"
        ),
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("-p", "--port", default=5000, help="port to run server on")
    parser.add_argument("--debug", action="store_true", help="debug the server")
    parser.add_argument("--version", action="store_true", help="print version and exit")
    parser.add_argument("--proj-name", default="PROJ_OIA", help="name of project")
    parser.add_argument(
        "--command", default="bash", help="Command to run in the terminal"
    )
    parser.add_argument(
        "--cmd-args",
        default="",
        help="arguments to pass to command (i.e. --cmd-args='arg1 arg2 --flag')",
    )
    args = parser.parse_args()
    if args.version:
        print(__version__)
        exit(0)
    print(f"serving on http://127.0.0.1:{args.port}")
    app.config["proj_name"] = args.proj_name
    app.config["cmd"] = [args.command] + shlex.split(args.cmd_args)
    socketio.run(app, host="0.0.0.0", debug=args.debug, port=int(args.port))


if __name__ == "__main__":
    main()
