import pyxtermjs.app as xterm

app = xterm.app

# --------------------Please change this section accordingly -----------------
app.config["proj_name"] = "PROJ_MYPROJ"

#app.config["command"] = "env_36/bin/python3"
#app.config["cmd-args"] = "app/helloworld.py"
app.config['cmd'] = ['env_36/bin/python3', 'app/helloworld.py']
# ------------------ End section ---------------------------------------------

if __name__ == '__main__':
    #xterm.socketio.run(app, port=80)
    app.run()



