FROM python:3.6-buster

RUN apt-get update && apt-get install -y vim
RUN pip install pyxtermjs uwsgi gevent gevent-websocket
COPY pyxtermjs.ini /

COPY env_36 /env_36
COPY pyxtermjs /pyxtermjs
COPY app /app


EXPOSE 80

CMD ["uwsgi", "--ini", "/pyxtermjs.ini"]
#CMD ["python", "pyxtermjs/app.py", "--proj-name", "PROJ_MYPROJ", "--command", "env_36/bin/python3", "--cmd-args", "app/helloworld.py", "-p", "80"]